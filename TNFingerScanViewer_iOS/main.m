//
//  main.m
//  TNFingerScanViewer_iOS
//
//  Created by atit on 7/17/2557 BE.
//  Copyright (c) 2557 atit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
