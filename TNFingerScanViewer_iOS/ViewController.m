//
//  ViewController.m
//  TNFingerScanViewer_iOS
//
//  Created by atit on 7/17/2557 BE.
//  Copyright (c) 2557 atit. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSURL   *requestURL;
    BOOL    isSelectTiemAttendant;
    BOOL    isSetMonth;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    isSelectTiemAttendant   = NO;
    isSetMonth              = NO;
    
    [self.webView setDelegate:self];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadWebViewUrl:@"https://id.thinknet.co.th/user/checkSession?url=http%3A%2F%2Fmy.thinknet.co.th%2Fauth"];
    
}

-(void)loadWebViewUrl:(NSString*)url {
    //url = [url stringByAddingPercentEscapesUsingEncoding:NSStringEncodingConversionExternalRepresentation];
    requestURL = [[NSURL alloc] initWithString:url];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:requestURL]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    NSString *url = [[NSString alloc] initWithFormat:@"%@",[[webView request] URL] ];
    NSLog(@"%@",url);
    
    if([url isEqualToString:@"https://id.thinknet.co.th/user/loginForm?url=http://my.thinknet.co.th/auth"]){
        
        [webView stringByEvaluatingJavaScriptFromString:@"javascript:(function()"
         "{document.getElementById('user_login').value = 'atit';"
         "document.getElementById('user_pass').value = 'At123456';"
         "document.getElementById('loginform').submit();"
         "})()"];
    }
    else if([url isEqualToString:@"http://my.thinknet.co.th/index.php"]){
        if(!isSelectTiemAttendant){
            NSLog(@"Select Time Attendant");
            /*
             webView.loadUrl("javascript:" + "(function(){" +
             "selectApplication('Timeattendant');" +
             "})()");
             */
            [self performSelector:@selector(selectTimeAttendant) withObject:self afterDelay:2.0f];
            isSelectTiemAttendant = YES;
        }
    }
    else if([url isEqualToString:@"http://hris.thinknet.co.th/employee/attendant.php"]){
        [self prepareData];
    }
    
}

-(void)selectTimeAttendant{
    [self.webView stringByEvaluatingJavaScriptFromString:@"javascript:(function(){"
     "selectApplication('Timeattendant');"
     "})()"];
    
    [self performSelector:@selector(gotoTimeAttendant) withObject:self afterDelay:2.0f];
}

-(void)gotoTimeAttendant{
    [self loadWebViewUrl:@"http://hris.thinknet.co.th/employee/attendant.php"];
}

-(void)prepareData{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSString *jsCommand = [[NSString alloc] initWithFormat:@"javascript:(function(){"
                              "document.getElementById('year').value = '%ld';"
                              "document.getElementById('month').value = '%ld';"
                              "document.getElementById('amount').value = '1';"
                              "document.getElementsByName('frm')[0].submit();"
                              "})()",(long)year,month];
    NSLog(jsCommand);
    
    [self.webView stringByEvaluatingJavaScriptFromString:jsCommand];
    [self performSelector:@selector(addPlugin) withObject:self afterDelay:2.0f];
}

-(void)addPlugin{
    NSString *jsCommand = [[NSString alloc] initWithFormat:@"javascript:(function(){"
                           "var source = document.createElement(\"script\");"
                           "source.type = \"text/javascript\";"
                           "source.src = \"http://code.jquery.com/jquery-1.11.1.min.js\";"
                           "document.getElementsByTagName('head')[0].appendChild(source);"
                           
                           "var code = document.createElement(\"script\");"
                           "code.type = \"text/javascript\";"
                           "code.src = \"http://yourjavascript.com/74481170211/calculate-time.js\";"
                           "document.getElementsByTagName('head')[0].appendChild(code);"
                           
                           "})()"];
    [self.webView stringByEvaluatingJavaScriptFromString:jsCommand];
}
@end
