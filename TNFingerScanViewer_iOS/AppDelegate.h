//
//  AppDelegate.h
//  TNFingerScanViewer_iOS
//
//  Created by atit on 7/17/2557 BE.
//  Copyright (c) 2557 atit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

