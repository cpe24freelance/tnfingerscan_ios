//
//  ViewController.h
//  TNFingerScanViewer_iOS
//
//  Created by atit on 7/17/2557 BE.
//  Copyright (c) 2557 atit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;


@end

